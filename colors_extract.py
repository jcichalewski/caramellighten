from pickle import FRAME
import cv2

FRAME_DURATION = 1/29.97
COLOR_PROBE_LOCATION = (1, 1)


def get_colors(vid):
    count = 0
    success, frame = vid.read()
    while success:
        count += 1
        yield frame[COLOR_PROBE_LOCATION]
        success, frame = vid.read()


def color_difference_enough(intensity, prev_intensity):
    diff_sum = 0
    for color1, color2 in zip(intensity, prev_intensity):
        diff = abs(color1 - color2)
        diff_sum += diff
    diff_sum /= 3 
    
    return True if diff_sum > 50 else False


def get_color_changes_with_timestamp(colors, compare_function):
    prev_intensity = next(colors)

    for frame_number, intensity in enumerate(colors, start=1):
        if compare_function(intensity, prev_intensity):
            prev_intensity = intensity
            time_now = FRAME_DURATION * frame_number
            yield time_now, intensity


vid = cv2.VideoCapture('lights.mp4')

with open('colors.csv', 'w') as f:
    for time, color in get_color_changes_with_timestamp(get_colors(vid), color_difference_enough):
        b, g, r = color
        f.write(f'{time:.4f},{r},{g},{b}\n')


