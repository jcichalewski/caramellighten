import asyncio 
import time

from subprocess import Popen

from pywizlight import wizlight, PilotBuilder, discovery


color_timeseries = []

with open('colors.csv', 'r') as f:
    for line in f:
        t, r, g, b = line.strip().split(',')
        color_timeseries.append((float(t), (int(r), int(g), int(b))))


def vid():
    DETACHED_PROCESS = 0x00000008
    cmd = [
        'C:\Program Files\mpv\mpv.exe',
        '--fs',
        '--no-video',
        'lights.mp4'
    ]
    p = Popen(
        cmd, shell=False, stdin=None, stdout=None, stderr=None,
        close_fds=True, creationflags=DETACHED_PROCESS,
    )


async def main():
    light = wizlight('192.168.1.112')
    await light.turn_on(PilotBuilder(brightness=255))

    try:
        vid()  # play vid
        t0 = time.time()
        while color_timeseries:
            t, color = color_timeseries.pop(0)
            while (elapsed := time.time() - t0) < t:  # wait for its turn
                time.sleep(0.002)
        
            await light.turn_on(PilotBuilder(rgb=color, brightness=255, speed=200))
    except KeyboardInterrupt:
        await light.turn_off()

    
loop = asyncio.get_event_loop()
loop.run_until_complete(main())

